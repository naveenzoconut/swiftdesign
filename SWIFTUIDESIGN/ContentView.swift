//
//  ContentView.swift
//  SWIFTUIDESIGN
//
//  Created by Naveen Pokala on 01/06/21.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, world!")
            .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
